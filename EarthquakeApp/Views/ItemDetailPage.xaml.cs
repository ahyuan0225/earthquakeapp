﻿using EarthquakeApp.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;

namespace EarthquakeApp.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}