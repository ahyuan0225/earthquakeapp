﻿using DynamicData.Binding;
using EarthquakeApp.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace EarthquakeApp.ViewModels
{
    public class LatestViewModel
    {
        public IEnumerable<LatestData> DataList { get; set; }

        public LatestViewModel()
        {
            DataList = GetDataList();
        }

        public IEnumerable<LatestData> GetDataList()
        {
            return new List<LatestData>
            {
                new LatestData()
                {
                    Mag = 3.5,
                    Region = "Puerto Rico Region",
                    Depth = 17.4
                },
                new LatestData()
                {
                    Mag=4.6,
                    Region="Philippine Islands Region",
                    Depth=22.2
                },
                new LatestData()
                {
                    Mag = 5.4,
                    Region = "Fiji Islands Region",
                    Depth = 385.9
                },
                new LatestData()
                {
                    Mag = 3.5,
                    Region = "Southern California",
                    Depth = 7.2
                },
                new LatestData()
                {
                    Mag = 3.0,
                    Region = "Central California",
                    Depth = 1.6
                },
                new LatestData()
                {
                    Mag = 4.0,
                    Region = "South of Alaska",
                    Depth =  13.5
                }
            };
        }
    }
}
