﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EarthquakeApp.Models
{
    public class LatestData
    {
        public double Mag { get; set; } //magnitude
        public string Region { get; set; }
        public double Depth { get; set; }
        public string DepthMi => Depth + " Mi";
    }
}
